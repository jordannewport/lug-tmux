# tmux

## A terminal multiplexer

Jordan Newport

Mines Linux User Group

---
# You can follow along!

* tmux is available from most distros' package repositories
* for extra spice, do it on a remote instead

---
# Terminal Multiplexing: What is it?

* "Enables a number of terminals to be created, accessed, and controlled from a single screen"

---
# Terminal Multiplexing: What can you do?

* Multiple terminals in one screen
* long-running processes that you don't want to keep open
* multiple accesses to one terminal

---
# Terminal Multiplexing: Why would you do it?

* You're in an ssh session or tty and you...
    * want two terminals in one session
    * have a long-running command you want to run
    * want to copy/paste things
* Your window manager just isn't cutting it for you anymore

---
# Terminal Multiplexing: What are some alternatives to tmux?

* GNU screen (GPL licensed; tmux is ISC)
* Nothing else good

---
# Commands: basics

These work for tmux 3.0a; I haven't tested against anything else

* You may eventually wish to rebind these, because they are bad
* C-d to quit tmux while nothing is running
* C-b is the "prefix key"

From here on out, everything will be prefixed with this key

* & to kill window at any time
* ? to see all the keybindings
    * q to close

---
# Multiple Terminals in one screen

---
# Tiling

* Tile windows: "panes" in tmux-speak
    * Tile horizontally: %
    * Tile vertically: "
* C-d or x to close one pane
    * & will kill whole window still

---
# Navigating Panes

* <arrow keys>: move between panes
* o to select the next pane
* ; to select most recent pane
* q to see numbers of panes
    * press a number to move to that pane

---
# Arranging Panes

* M-[1-5] to arrange panes in a preset layout
    * M is Alt on most keyboards/layouts
* { or } to swap panes
* C-o (or M-o) to rotate pane layout
* C-<arrow keys> (or M-<arrow keys>) to resize panes
* z to "zoom" (fullscreen) a pane

---
# Windows

* tmux panes are to windows as tmux windows are to workspaces
* They are numbered in order, but you can rename them

* Creating windows
    * c to create a new window
    * ! to break the current pane into a new window
* Destroying windows
    * & actually kills a *window*, not a whole tmux session

---
# Navigating Windows

* [0-9] switches to that window
* w gives you a graphical window switcher
* n and p cycle through windows
* l gives you most recent window
* i gives you information if you ever get confused

---
# Arranging Windows

* . to move a window to a different number
* , to rename a window, if you really want that
* :join-pane -t :N to send pane to window N
* :join-pane -s :N to grab pane from window N

---
# Long-Running Processes

---
# Sessions

* tmux can store sessions in the background and recall them
    * by default, they are numbered from 0
* d to "detach" a session to the background
* \`tmux attach\` in shell to return to the most recent tmux session
    * -d <name> to attach to session <name> instead of the most recent

---
# Managing Sessions

* \`tmux list-sessions\` to see all your sessions
* $ to rename a session
* ( and ) to cycle sessions
* s for a graphical session switcher

---
# Paste buffers

* recent versions of tmux give you a clipboard
    * \[ to enter "copy mode"
    * navigate (arrow keys, maybe vim bindings) to where you want to start copying
    * <space> to start copying
    * move around; it should highlight the text to be copied
    * <enter> when you're done copying
    * ] to paste
* if you wanted, you could probably interface with xclip/xsel/whatever Wayland has

---
# Multiple Accesses to One Terminal

* Two different people can attach to the same tmux session
    * Useful for helping people over a network

---
# Appearance

* You can set colors for things (\`man tmux\` spells it "colour")

* :set -w window-style bg=blue
* :set -g status-style "bg=red"
* :show-options -w
* much more under set-option and the STYLES section in the man page

---
# Ecosystem

* tmux is a good player in the broader UNIX ecosystem
* Kakoune, for example, allows:
    tmux-terminal-horizontal kak -c %val{session}
    in order to start a new kakoune window

---
# Configuration

* configuration belongs in ~/.tmux.conf
* Comparable to a "tmuxrc"
* bind-key j command-prompt -p "join pane from:"  "join-pane -s :'%%'"
* bind-key s command-prompt -p "send pane to:"  "join-pane -t :'%%'"
* tmux source-file ~/.tmux.conf from inside tmux

* so many more options available--see the man page

---
# Credits

* The (extremely long--2945 in FreeBSD, which uses 80-char lines) tmux man page
    * yes, it is longer than sudoers!
* http://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/
* so many StackExchange questions

---
# Copyright Notice

This presentation was from the **Mines Linux Users Group**. A mostly-complete
archive of our presentations can be found online at https://lug.mines.edu.

Individual authors may have certain copyright or licensing restrictions on their
presentations. Please be certain to contact the original author to obtain
permission to reuse or distribute these slides.
